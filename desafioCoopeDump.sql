CREATE DATABASE  IF NOT EXISTS `coopersystem` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `coopersystem`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: coopersystem
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CLIENTE`
--

DROP TABLE IF EXISTS `CLIENTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CLIENTE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATAOPERACAO` datetime DEFAULT NULL,
  `NOME` varchar(45) NOT NULL,
  `CPF` varchar(45) NOT NULL,
  `UPDATE_AT` datetime DEFAULT NULL,
  `TELEFONE` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CLIENTE`
--

LOCK TABLES `CLIENTE` WRITE;
/*!40000 ALTER TABLE `CLIENTE` DISABLE KEYS */;
INSERT INTO `CLIENTE` VALUES (4,NULL,'Paulo Henrique Ferreira dias','02807959121',NULL,'6195578343'),(5,NULL,'Fernando souza','03878241119',NULL,'6195578343'),(6,NULL,'Maria do Socorro','03878241119',NULL,'6195578343'),(7,NULL,'Paulo silva','99999999999',NULL,'61955783433'),(8,NULL,'joazinho','98989898989',NULL,'61955783439'),(9,NULL,'Maria','90809809889',NULL,'80988898989'),(10,NULL,'Julia','39393902939',NULL,'61955783439'),(11,NULL,'Matias','52345252525',NULL,'90808999999'),(12,NULL,'Jericó','09090909090',NULL,'61955783439'),(13,NULL,'mateus solando','98909090909',NULL,'90909090909'),(14,NULL,'UMC','02807959121',NULL,'98493849384'),(15,NULL,'maria aparecida','93949394394',NULL,'61955783433'),(16,NULL,'Paulo fasdfa','42153525252',NULL,'23232322455'),(17,NULL,'ffasfas','32412431414',NULL,'24123414314'),(18,NULL,'sadfa','32412414124',NULL,'23141241411'),(19,NULL,'sdfasf','23414142141',NULL,'31241241414'),(20,NULL,'sdasasfa','42314124124',NULL,'32414141414'),(21,NULL,'saa','42121132414',NULL,'32112413412'),(22,NULL,'sfas','43523523523',NULL,'23543252352'),(24,NULL,'asfdad','45452352345',NULL,'42523523525'),(25,NULL,'mais um teste','24352452452',NULL,'42252352352'),(26,NULL,'mais um teste','43253452352',NULL,'43112412411'),(27,NULL,'teste vamos lá','92085923582',NULL,'42542353242'),(28,NULL,'dsafsdfasfas','42452353523',NULL,'24234523523'),(29,NULL,'adfasf','41234123412',NULL,'12314123412'),(30,NULL,'Paulo Bruno','41234123413',NULL,'61955783434'),(31,NULL,'machopi','25252345245',NULL,'12412433434'),(32,NULL,'ppdkd','24312412342',NULL,'24235234523'),(33,NULL,'otaviano','24523523542',NULL,'34124124124'),(34,NULL,'Mateus Ceará','42312412412',NULL,'12312412134'),(35,NULL,'rwfasdfa','42235325423',NULL,'42352542352'),(36,NULL,'fdafa','88989899999',NULL,'88809808080'),(38,NULL,'teste','43523452352',NULL,'32534523452'),(39,NULL,'Susaze morais','25425323525',NULL,'24523452352');
/*!40000 ALTER TABLE `CLIENTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente_email`
--

DROP TABLE IF EXISTS `cliente_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente_email` (
  `id` bigint(20) NOT NULL,
  `email_id` int(11) NOT NULL,
  UNIQUE KEY `UK_cs9l4xsuhyx6twkfx9bvx5vsi` (`email_id`),
  KEY `FK1mpr55iph7rmu2nqid7rhehap` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente_email`
--

LOCK TABLES `cliente_email` WRITE;
/*!40000 ALTER TABLE `cliente_email` DISABLE KEYS */;
INSERT INTO `cliente_email` VALUES (38,1),(39,2);
/*!40000 ALTER TABLE `cliente_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente_endereco`
--

DROP TABLE IF EXISTS `cliente_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente_endereco` (
  `id` bigint(20) NOT NULL,
  `endereco_id` int(11) NOT NULL,
  UNIQUE KEY `UK_eaijuk6dr5awjv26fvex140lv` (`endereco_id`),
  KEY `FKmyi2i5gnh57gw7f8ywu27eoll` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente_endereco`
--

LOCK TABLES `cliente_endereco` WRITE;
/*!40000 ALTER TABLE `cliente_endereco` DISABLE KEYS */;
INSERT INTO `cliente_endereco` VALUES (24,1),(25,2),(26,3),(27,4),(28,5),(28,6),(29,7),(30,8),(30,9),(30,10),(32,11),(32,12),(32,13),(33,14),(34,15),(34,16),(35,17),(36,18),(38,19),(39,20);
/*!40000 ALTER TABLE `cliente_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `principal` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` VALUES (1,'s@gmail.com',NULL),(2,'su@gmail.com',NULL);
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `endereco`
--

DROP TABLE IF EXISTS `endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `endereco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bairro` varchar(255) DEFAULT NULL,
  `cep` varchar(255) DEFAULT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  `localidade` varchar(255) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `principal` bit(1) DEFAULT NULL,
  `uf` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `endereco`
--

LOCK TABLES `endereco` WRITE;
/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;
INSERT INTO `endereco` VALUES (1,'Parque da Barragem Setor 08','72910-049','','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(2,'Parque da Barragem Setor 08','72910-049','','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(3,'Parque da Barragem Setor 08','72910-049','','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(4,NULL,NULL,'Casa 15',NULL,NULL,NULL,NULL),(5,'Parque da Barragem Setor 08','72910-049','','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(6,NULL,NULL,'dddadasdfa',NULL,NULL,NULL,NULL),(7,'Parque da Barragem Setor 08','72910-049','casa 18','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(8,'Parque da Barragem Setor 08','72910-049','casa 28','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(9,'Parque da Barragem Setor 08','72910-049','casa 29','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(10,'Parque da Barragem Setor 08','72910-025','casa 90','Águas Lindas de Goiás','Quadra 23 Conjunto A',NULL,'GO'),(11,'Parque da Barragem Setor 08','72910-049','apartamento 140','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(12,'Areal (Águas Claras)','71950-904','setor norte','Brasília','QS 1 Rua 210 Lote 40',NULL,'DF'),(13,'Asa Norte','70715-900','Asa norte','Brasília','SCN Quadra 5 Bloco A',NULL,'DF'),(14,'Asa Norte','70715-900','setor norte','Brasília','SCN Quadra 5 Bloco A',NULL,'DF'),(15,'Parque da Barragem Setor 08','72910-049','casa 94','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(16,'Asa Norte','70715-900','asa norte','Brasília','SCN Quadra 5 Bloco A',NULL,'DF'),(17,'Parque da Barragem Setor 08','72910-049','faf','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(18,'Parque da Barragem Setor 08','72910-049',NULL,'Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(19,'Parque da Barragem Setor 08','72910-049','ddd','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO'),(20,'Parque da Barragem Setor 08','72910-049','dd','Águas Lindas de Goiás','Quadra 45 Conjunto A',NULL,'GO');
/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (40);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'coopersystem'
--

--
-- Dumping routines for database 'coopersystem'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-16 12:49:43
