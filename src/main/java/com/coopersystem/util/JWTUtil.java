package com.coopersystem.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class JWTUtil {

    private static String key = "SECRET_TOKEN";

    public static final String TOKEN_HEADER = "Authorization";

    @SuppressWarnings("deprecation")
	public static String create(String subject) {
    	
    	Calendar calendar = Calendar.getInstance();
//    	 Map<String, Object> tokenData = new HashMap<>();
    	 
//    	 tokenData.put("userID", subject);
    	 
    	    final Jws<Claims> tokenData = Jwts.parser().setSigningKey(key).parseClaimsJws(subject);
     	final JwtBuilder jwtBuilder = Jwts.builder();

    	    jwtBuilder.setClaims(tokenData.getBody());
    	    calendar.add(Calendar.MILLISECOND, 1);
    	    jwtBuilder.setExpiration(calendar.getTime());
//    	    return jwtBuilder.signWith(SignatureAlgorithm.HS512, key).compact();
    	
//    	JwtBuilder jwtBuilder = Jwts.builder();
//        jwtBuilder.setExpiration(calendar.getTime());
//        jwtBuilder.setClaims(tokenData);
        
        return jwtBuilder.signWith(SignatureAlgorithm.HS512, key).compact();
       
//            return Jwts.builder()
//                .setSubject(subject)
//                .signWith(SignatureAlgorithm.HS512, key)
//                .compact();
    }

    public static Jws<Claims> decode(String token){
        return Jwts.parser().setSigningKey(key).parseClaimsJws(token);
    }

}
