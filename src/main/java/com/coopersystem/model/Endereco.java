package com.coopersystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.coopersystem.util.Constantes;


@Entity
@Table(name = "ENDERECO", schema = Constantes.BANCO)
public class Endereco implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Integer id;
	
	@Column(name = "CEP")
	private  String cep;
	
	@Column(name = "LOGRADOURO")
	private  String logradouro;
	
	@Column(name = "COMPLEMENTO")
	private  String complemento;
	
	@Column(name = "BAIRRO")
	private  String bairro;
	
	@Column(name = "LOCALIDADE")
	private  String localidade;
	
	@Column(name = "UF")
	private  String uf;
	
	@Column(name = "PRINCIPAL")
	private  Boolean principal;
	
	public Endereco() {
		
	}
	
	
	public Endereco(String cep, String logradouro, String complemento, String bairro, String localidade, String uf,
			 String ibge, String gia) {
		super();
		this.cep = cep;
		this.logradouro = logradouro;
		this.complemento = complemento;
		this.bairro = bairro;
		this.localidade = localidade;
		this.uf = uf;
	}
	
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getLocalidade() {
		return localidade;
	}
	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Boolean getPrincipal() {
		return principal;
	}
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

    
}