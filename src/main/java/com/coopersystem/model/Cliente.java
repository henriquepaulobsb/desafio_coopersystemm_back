package com.coopersystem.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.coopersystem.util.Constantes;

@Entity
@Table(name = "CLIENTE", schema = Constantes.BANCO)
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long id;

	@NotBlank
	@Column(name = "NOME")
	private String name;
	
	@NotBlank
	@Column(name = "CPF")
	private String cpf;
	
	@NotBlank
	@Column(name = "TELEFONE")
	private String telefone;
	
//	@NotBlank
//	@Column(name = "EMAIL")
//	private String email;

	@CreatedDate
	@Column(name = "DATAOPERACAO")
	private Date dataOperação;
	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	  @JoinTable(name="CLIENTE_ENDERECO",
	             joinColumns={@JoinColumn(name = "ID")},
	             inverseJoinColumns={@JoinColumn(name = "ENDERECO_ID")})
	  private List<Endereco> enderecos = new ArrayList<>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	  @JoinTable(name="CLIENTE_EMAIL",
	             joinColumns={@JoinColumn(name = "ID")},
	             inverseJoinColumns={@JoinColumn(name = "EMAIL_ID")})
	  private List<Email> emails = new ArrayList<>();

	@LastModifiedDate
	@Column(name = "UPDATE_AT")
	private Date updatedAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}

	public Date getDataOperação() {
		return dataOperação;
	}

	public void setDataOperação(Date dataOperação) {
		this.dataOperação = dataOperação;
	}

	public List<Endereco> getEnderecos() {
		if (enderecos == null) {
			enderecos = new ArrayList<>();
		}
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
