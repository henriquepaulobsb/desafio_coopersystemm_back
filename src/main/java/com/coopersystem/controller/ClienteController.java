package com.coopersystem.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.coopersystem.dao.ClienteRepository;
import com.coopersystem.model.Cliente;
import com.coopersystem.model.UserLogged;

//@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class ClienteController {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	 @GetMapping("/clientes")
	    public List<Cliente> getAllNotes() {
		 Iterable<Cliente> systemlist = clienteRepository.findAll();
	        return (List<Cliente>) systemlist;
	  }
	 
	 @GetMapping("/clientePaginada")
	    public ResponseEntity<?> getAllNotesPaginada(Pageable pageable) {
	        return new ResponseEntity<>(clienteRepository.findAll(pageable),HttpStatus.OK);
	  }

	 
	@SuppressWarnings("static-access")
	@PostMapping("/cliente")
	    public ResponseEntity<?> createNoteNew(@RequestBody @Valid Cliente cliente) {
		 try {
			 clienteRepository.save(cliente);
			 return new ResponseEntity<>(clienteRepository.save(cliente),HttpStatus.CREATED).ok("Cadastrato efetuado com sucesso!");
		    } catch (Exception e) {
			return ResponseEntity.badRequest().body("Erro ao cadastrar tente novamente mais tarde.");
		   }
	    }
	
	    @PutMapping("/update")
	    public ResponseEntity<?> upDateSystem(@RequestBody Cliente cliente) {
		 try {
			 clienteRepository.saveAndFlush(cliente);
			 return ResponseEntity.ok("Alteção efetuado com sucesso!");
		 } catch (Exception e) {
			return ResponseEntity.badRequest().body("Erro ao alterar tente novamente mais tarde.");
		  }
	    }
	
	 @GetMapping("/cliente/{id}")
	    public Optional<Cliente> getNoteById(@PathVariable(value = "id") Long noteId) {
		 Optional<Cliente> systemlist = clienteRepository.findById(noteId);
	        return systemlist;
	    }
	
	@GetMapping("/search")
	public Page<Cliente> search(
			@RequestParam("searchTerm") String searchTerm,
            @RequestParam(value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "10") int size) {
		
		 PageRequest pageRequest = PageRequest.of(page,size, Sort.Direction.ASC, "name");
	        return clienteRepository.search(searchTerm.toLowerCase(), pageRequest);
		
//        return service.search(searchTerm, page, size);

    }
	
	@SuppressWarnings("static-access")
	@PostMapping("/login")
	public ResponseEntity<?> loginUser(@RequestBody Cliente credentials) {
//	    if(this.USERNAME.equals(credentials.getName()) && this.PASSWORD.equals(credentials.getPassword())){
//	      	String token = "token";
//	        String token = JWTUtil.create(credentials.getName());
	        
	        UserLogged me = new UserLogged();
	        me.setUsername(credentials.getName());
//	        me.setToken(token);
//	        return (ResponseEntity<>) ResponseEntity.badRequest();
	        return new ResponseEntity<>(me,HttpStatus.ACCEPTED);
//	    }else{
//	        return new ResponseEntity<>(HttpStatus.ACCEPTED);
//	    }
	    
//	    return new ResponseEntity<>(me,HttpStatus.ACCEPTED);
	}
	


}
