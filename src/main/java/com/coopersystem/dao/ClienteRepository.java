package com.coopersystem.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.coopersystem.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Long> {
	
	@Query("FROM Cliente c " +
	           "WHERE LOWER(c.name) like %:searchTerm% ")
	    Page<Cliente> search(@Param("searchTerm") String searchTerm, Pageable pageable);

}
